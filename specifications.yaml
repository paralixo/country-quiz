openapi: 3.0.0
info:
  title: Country quiz
  description: "This is a webservice built for generating and answering to questions about differents countries in the world. The questions are generated thanks to the api of <a href='https://restcountries.com'><strong>restcountries.com<strong></a>."
  version: 1.0.0
paths:
  /quiz:
    post:
      security:
        - basic: [ ]
      description: "Create a new quiz with given parameters (one zone, one difficulty and/or multiple themes) and return the generated questions (10) related to the quiz. All parameters are required but will not be considered if empty.<br>
                   <strong>Zone</strong> is about the countries selected in the quiz (in the question and in the possible answers). <strong>Theme</strong> is about what the question ask for and difficulty is an arbitrary filter for countries.<br><br>
                   Possible <strong>difficulties</strong> are : `Easy`, `Medium` and `Hard`.<br>
                   Possible <strong>themes</strong> are : `Capital`, `Flag`, `Country` and `Coat of Arms`.<br>
                   Possible <strong>zones</strong> are : continents names or subregions (for subregions names, please refer to <a href='https://restcountries.com'><strong>restcountries.com<strong></a>).<br>"
      summary: Create a new quiz
      tags: [Quiz]
      operationId: src.api.create_quiz
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/QuizParameters'
      responses:
        200:
          description: Return the created quiz with the associated questions
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Quiz'
        400:
          $ref: '#/components/responses/BadRequestError'
        401:
          $ref: '#/components/responses/UnauthorizedError'
        404:
          $ref: '#/components/responses/NotFoundError'
        500:
          $ref: '#/components/responses/ServerError'
  /response:
    post:
      security:
        - basic: [ ]
      description: "Create a response to a given question (through question id). Both parameters in the body are required.
                    You can answer a question only once.<br><br>
                    If the question doesn't exists or the response isn't available in the question an error will occured, else the API will return if the response is valid and the expected response."
      summary: Create a response
      tags: [ Response ]
      operationId: src.api.create_response
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Response'
      responses:
        200:
          description: Return the validity of the response
          content:
            application/json:
              schema:
                type: object
                properties:
                  user_response_valid:
                    type: boolean
                  expected_response:
                    type: string
                example:
                  user_response_valid: true
                  expected_response: Paris
        400:
          $ref: '#/components/responses/BadRequestError'
        401:
          $ref: '#/components/responses/UnauthorizedError'
        404:
          $ref: '#/components/responses/NotFoundError'
        500:
          $ref: '#/components/responses/ServerError'
  /user/{user_id}:
    get:
      security:
        - basic: [ ]
      description: "Get a user with all his quiz and the user responses (if any) to the questions.<br>
                    As long as your are connected, you can fetch data from any user (to simulate the view of other profiles).<br><br>
                    The value `grade` in the quiz is dynamically generated from the user responses (if any else 0)."
      summary: Get a user
      tags: [ User ]
      operationId: src.api.get_user
      parameters:
        - name: user_id
          in: path
          description: Id of the desired user
          required: true
          schema:
            type: string
          example: fea19441-37d4-11ec-9230-e14bd23ebe00
      responses:
        200:
          description: Return the corresponding user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        400:
          $ref: '#/components/responses/BadRequestError'
        401:
          $ref: '#/components/responses/UnauthorizedError'
        404:
          $ref: '#/components/responses/NotFoundError'
        500:
          $ref: '#/components/responses/ServerError'
  /user:
    post:
      description: "Signup a new user account with an email and a password. You can't have two accounts with the same email."
      summary: Signup a new user account
      tags: [ User ]
      operationId: src.api.signup
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              required: [email, password]
              properties:
                email:
                  type: string
                  description: Email of the user
                password:
                  type: string
                  description: Password of the user
              example:
                email: florian.lafuente@ynov.com
                password: azerty1234
      responses:
        200:
          description: Return the id of the new user
          content:
            application/json:
              schema:
                type: string
                example: fea19441-37d4-11ec-9230-e14bd23ebe00
        400:
          $ref: '#/components/responses/BadRequestError'
        404:
          $ref: '#/components/responses/NotFoundError'
        500:
          $ref: '#/components/responses/ServerError'
  /countries:
    post:
      security:
        - basic: [ ]
      description: Import countries into our database from the <a href='https://restcountries.com'><strong>rest countries API<strong></a>
      summary: Import countries
      tags: [ Countries ]
      operationId: src.api.import_countries
      responses:
        200:
          description: Return the validity of the response
          content:
            application/json:
              schema:
                type: string
                example: L'import s'est effectué correctement (x éléments importés)
        401:
          $ref: '#/components/responses/UnauthorizedError'
        500:
          $ref: '#/components/responses/ServerError'

components:
  securitySchemes:
      basic:
        type: http
        scheme: basic
        x-basicInfoFunc: src.user.basic_auth
  responses:
    UnauthorizedError:
      description: Authentication information is missing or invalid
      headers:
        Authorization:
          schema:
            description: Basic authentication information
            type: string
            example: Basic <username & password Base64 encoded>
    NotFoundError:
      description: Unknow ressource or parameters
    ServerError:
      description: An error occurred while processing your request, please contact one administrator
    BadRequestError:
      description: Something is missing in your request
  schemas:
    User:
      type: object
      required: [id, email, quiz]
      properties:
        id:
          type: string
          description: User id
        email:
          type: string
          description: User email
        quiz:
          type: array
          description: List of quiz of the user
          items:
            $ref: '#/components/schemas/UserQuiz'
      example:
        id: fea19441-37d4-11ec-9230-e14bd23ebe00
        email: florian.lafuente@ynov.com
        quiz:
          - themes: [ capital, flag ]
            theme_zone: Europe
            difficulty: Easy
            grade: 1
            questions:
              - id: 1
                label: 'What is the capital of France?'
                options: [ Paris, Madrid, London, Rome ]
                user_response: Madrid
                user_response_valid: false
              - id: 2
                label: 'What is the capital of Spain?'
                options: [ Paris, Madrid, London, Rome ]
                user_response: Madrid
                user_response_valid: true
              - id: 3
                label: 'What is the capital of Italy?'
                options: [ Paris, Madrid, London, Rome ]
                user_response: null
                user_response_valid: null
    UserQuiz:
      allOf:
        - $ref: '#/components/schemas/Quiz'
        - type: object
          properties:
            grade:
              type: integer
              description: Number of good answers for the user (max is 10)
            questions:
              type: array
              description: List of generated questions with answers
              items:
                $ref: '#/components/schemas/Question'
          example:
            themes: [ capital, flag ]
            theme_zone: Europe
            difficulty: Easy
            grade: 1
            questions:
              - id: 1
                label: 'What is the capital of France?'
                options: [ Paris, Madrid, London, Rome ]
                user_response: Madrid
                user_response_valid: false
              - id: 2
                label: 'What is the capital of Spain?'
                options: [ Paris, Madrid, London, Rome ]
                user_response: Madrid
                user_response_valid: true
              - id: 3
                label: 'What is the capital of Italy?'
                options: [ Paris, Madrid, London, Rome ]
                user_response: null
                user_response_valid: null
    Response:
      type: object
      required: [question_id, response]
      properties:
        question_id:
          type: number
          format: int64
          description: The answered question
        response:
          type: string
          description: The response
      example:
        question_id: 1
        response: Paris
    QuizParameters:
      type: object
      required: [difficulty, theme_zone, themes]
      properties:
        themes:
          type: array
          nullable: true
          description: List of themes for the quiz (questions about capitals, country name, flags, ...)
          items:
            type: string
        theme_zone:
          type: string
          nullable: true
          description: Region or subregion of the countries in the quiz
        difficulty:
          type: string
      example:
        themes: [ capital, flag ]
        theme_zone: Europe
        difficulty: Easy
    Quiz:
      type: object
      required: [themes, theme_zone, difficulty, questions]
      properties:
        themes:
          type: array
          nullable: true
          description: List of themes for the quiz (questions about capitals, country name, flags, ...)
          items:
            type: string
        theme_zone:
          type: string
          nullable: true
          description: Region or subregion of the countries in the quiz
        difficulty:
          type: string
        questions:
          type: array
          description: List of generated questions
          items:
            $ref: '#/components/schemas/Question'
      example:
        themes: [ capital, flag ]
        theme_zone: Europe
        difficulty: Easy
        questions:
          - id: 1
            label: 'What is the capital of France?'
            options: [ Paris, Madrid, London, Rome ]
    Question:
      type: object
      required: [label, options]
      properties:
        id:
          type: integer
          description: Question id
        label:
          type: string
          description: The generated question
        options:
          type: array
          description: List of possible options
          items:
            type: string
        user_response:
          type: string
          nullable: true
          description: Response made by the user (if not answered yet, it is null)
        user_response_valid:
          type: boolean
          nullable: true
          description: Is the user response the correct answer (if not answered yet, it is null)
      example:
        id: 1
        label: 'What is the capital of France?'
        options: [ Berlin, Paris, Amsterdam, Riga ]
        user_response: 'Amsterdam'
        user_response_valid: false