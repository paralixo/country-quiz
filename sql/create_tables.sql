CREATE TABLE `users` (
  `id` varchar(255) PRIMARY KEY,
  `email` varchar(255),
  `password` varchar(255)
);

CREATE TABLE `quiz` (
  `id` int PRIMARY KEY auto_increment,
  `user_id` varchar(255),
  `difficulty` varchar(255),
  `theme_zone` varchar(255)
);

CREATE TABLE `templates_questions` (
  `id` int PRIMARY KEY auto_increment,
  `format` varchar(255),
  `element_question` varchar(255),
  `theme_id` int
);

CREATE TABLE `questions` (
  `id` int PRIMARY KEY auto_increment,
  `template_question_id` int,
  `quiz_id` int,
  `value_question` varchar(255),
  `response` varchar(255),
  `option_2` varchar(255),
  `option_3` varchar(255),
  `option_4` varchar(255)
);

CREATE TABLE `responses` (
  `id` int PRIMARY KEY auto_increment,
  `question_id` int,
  `response` varchar(255)
);

CREATE TABLE `themes` (
  `id` int PRIMARY KEY auto_increment,
  `element_response` varchar(255),
  `label` varchar(255)
);

CREATE TABLE `themes_quiz` (
  `id` int PRIMARY KEY auto_increment,
  `quiz_id` int,
  `themes_id` int
);

CREATE TABLE `countries` (
  `code` varchar(3) PRIMARY KEY,
  `difficulty` varchar(255),
  `region` varchar(255),
  `subregion` varchar(255),
  `has_coa` boolean
);

ALTER TABLE `quiz` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `templates_questions` ADD FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`);

ALTER TABLE `questions` ADD FOREIGN KEY (`template_question_id`) REFERENCES `templates_questions` (`id`);

ALTER TABLE `questions` ADD FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`);

ALTER TABLE `questions` ADD FOREIGN KEY (`country_code`) REFERENCES  `countries` (`code`);

ALTER TABLE `responses` ADD FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`);

ALTER TABLE `themes_quiz` ADD FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`);

ALTER TABLE `themes_quiz` ADD FOREIGN KEY (`themes_id`) REFERENCES `themes` (`id`);