INSERT INTO users (id, email, password) VALUES
('fea19441-37d4-11ec-9230-e14bd23ebe00', 'soulei.seg@gmail.com', PASSWORD('abc123')),
('fea19680-37d4-11ec-9230-e14bd23ebe00', 'florian.lafuente@gmail.com', PASSWORD('def456')),
('fea196fc-37d4-11ec-9230-e14bd23ebe00', 'benoit.galmot@gmail.com', PASSWORD('ghi789'));

INSERT INTO quiz (id, user_id, difficulty, theme_zone) VALUES
(1, 'fea19441-37d4-11ec-9230-e14bd23ebe00', 'Medium', null),
(2, 'fea19680-37d4-11ec-9230-e14bd23ebe00', 'Medium', null),
(3, 'fea196fc-37d4-11ec-9230-e14bd23ebe00', 'Medium', null);

INSERT INTO themes (id, element_response, label) VALUES
(1, 'capital', 'Capital'),
(2, 'flags.png', 'Flags'),
(3, 'name.common', 'Country'),
(4, 'coatOfArms.png', 'Coat of Arms');

INSERT INTO templates_questions (id, format, element_question, theme_id) VALUES
(1, 'What is the capital of %value%?', 'name.common', 1),
(2, 'What is the flag of %value%?', 'name.common', 2),
(3, 'What is the coat of arms of %value%?', 'name.common', 4),
(4, 'What country does this coat of arms belong to? %value%', 'coatOfArms.png', 3),
(5, 'What country does this flag belong to? %value%', 'flags.png', 3),
(6, 'Which country has %value% as its capital ?', 'capital', 3),
(8, 'What is the capital of the country represented by this flag? %value%', 'flags.png', 1);

INSERT INTO countries (code, difficulty, region, subregion) VALUES
('FRA', 'Easy', 'Europe', 'Western Europe'),
('ESP', 'Easy', 'Europe', 'Western Europe'),
('DEU', 'Easy', 'Europe', 'Western Europe'); #Le code correspond au champ "cca3" de l'objet.

INSERT INTO questions (id, template_question_id, quiz_id, value_question, response, option_2, option_3, option_4) VALUES
(1, 1, 1, 'France', 'Paris', 'Londres', 'Washington', 'Rome'),
(2, 1, 2, 'Spain', 'Madrid', 'Londres', 'Washington', 'Rome'),
(3, 1, 3, 'Germany', 'Berlin', 'Amsterdam', 'Moscou', 'Stockholm');

INSERT INTO responses (id, question_id, response) VALUES
(1, 1, 'Paris'),
(2, 2, 'Rome'),
(3, 3, 'Amsterdam');

INSERT INTO themes_quiz (id, quiz_id, themes_id) VALUES
(1, 1, 1);