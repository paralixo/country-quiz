import connexion

from flask import redirect

from src.api import check_api_integrity
from src.extensions import mysql, mail


def register_extensions(flask_app):
    mysql.init_app(flask_app)
    mail.init_app(flask_app)


def create_application():
    flask_app = connexion.App(__name__)
    flask_app.add_api('specifications.yaml')
    flask_app.app.config.from_object('config.Config')
    register_extensions(flask_app.app)
    return flask_app


app = create_application()
application = app.app


@application.route('/')
def home():
    return redirect('ui')


# For dev purpose only
@application.route('/integrity')
def integrity():
    return check_api_integrity()


if __name__ == '__main__':
    app.run()
