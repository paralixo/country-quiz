import re

from src.db import execute_query, select_one, get_uuid


def sign_up(email, password) -> (str, str):
    error_message = None
    uuid = None
    accounts = execute_query('SELECT * FROM users WHERE email = %s', email)
    if accounts:
        error_message = 'Le compte existe déjà'
    elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
        error_message = 'Adresse mail invalide'
    else:
        uuid = get_uuid()
        execute_query('INSERT INTO users VALUES (%s, %s, PASSWORD(%s))', (uuid, email, password))
    return error_message, uuid


def basic_auth(email, password):
    account = select_one('SELECT * FROM users WHERE email = %s AND password = PASSWORD(%s)', (email, password))
    if account is None:
        return None
    user_id = account['id']
    return {'sub': user_id, 'scope': ''}
