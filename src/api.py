import random

import requests
from flask import jsonify

from src.constants import Api, NA
from src.db import execute_query, execute_many_queries, select_one, insert_one, insert_many
from src.mailing import confirm_signup
from src.user import sign_up
from src.countries import get_random_difficulty
from src.questions import fetch_data, get_api_element


def create_quiz(body, user):
    list_questions = fetch_data(body)
    if list_questions is None:
        return 'Not enough data to generate a quiz.', 500
    quiz_id = insert_one('INSERT INTO quiz (user_id, difficulty, theme_zone) VALUES (%s, %s, %s);', (user, body['difficulty'], body['theme_zone']))

    themes = body['themes']
    if len(themes) > 0:
        in_parameter = ', '.join(list(map(lambda x: '%s', themes)))
        matching_themes = execute_query("SELECT * FROM themes WHERE label IN (%s)" % in_parameter, tuple(themes))
        themes_quiz = map(lambda x: (quiz_id, x['id']), matching_themes)
        insert_many('INSERT INTO themes_quiz (quiz_id, themes_id) VALUES (%s, %s)', themes_quiz)

    insert_question_data = []
    for question in list_questions:
        insert_question_data.append((question['template_id'], quiz_id, question['value_question'], question['response'], question['option_2'], question['option_3'], question['option_4']))
    create_question = 'INSERT INTO questions (template_question_id, quiz_id, value_question, response, option_2, option_3, option_4) VALUES (%s, %s, %s, %s, %s, %s, %s);'
    ids = insert_many(create_question, insert_question_data)

    returned_questions = []
    for i in range(len(ids)):
        question = list_questions[i]
        responses = [question['response'], question['option_2'], question['option_3'], question['option_4']]
        random.shuffle(responses)

        returned_questions.append(({
            'label': question['label'],
            'options': responses,
            'id': ids[i]
        }))

    body['questions'] = returned_questions
    return body


def create_response(body):
    question_id, response = body['question_id'], body['response']
    question = select_one('SELECT id, response, option_2, option_3, option_4 FROM questions WHERE id = %s;', question_id)
    if question is None:
        return 'Question not found', 404

    existing_responses = execute_query('SELECT id FROM responses WHERE question_id = %s;', question['id'])
    if len(existing_responses) > 0:
        return 'You already answered this question', 404

    possible_responses = [
        question['response'],
        question['option_2'],
        question['option_3'],
        question['option_4']
    ]
    if response not in possible_responses:
        return 'Response not found', 404

    execute_query('INSERT INTO responses (question_id, response) VALUES (%s, %s);', (question_id, response))

    return {
        "user_response_valid": question['response'] == response,
        "expected_response": question['response']
    }


def get_user(user_id):
    user_info_quiz = execute_query('SELECT id, difficulty, theme_zone FROM quiz WHERE user_id = %s', user_id)
    user_info_questions = execute_query('SELECT oc.id, u.user_id, oc.quiz_id, tq.format, value_question, tq.element_question, oc.response, option_2, option_3, option_4, user.response AS user_response FROM questions AS oc INNER JOIN quiz u on u.user_id = %s LEFT OUTER JOIN responses user on oc.id = user.question_id INNER JOIN templates_questions tq on oc.template_question_id = tq.id WHERE oc.quiz_id = u.id',user_id)
    user_info = select_one('SELECT email, id FROM users WHERE id = %s', user_id)
    for question in user_info_questions:
        id_quiz = question['quiz_id']
        user_resp = question['user_response']
        opt_resp = question['response']
        opt_2 = question['option_2']
        opt_3 = question['option_3']
        opt_4 = question['option_4']
        format_question = question['format']
        element_question = question['value_question']
        question['label'] = format_question.replace('%value%', element_question)
        #Add [questions] in quiz
        for quiz in user_info_quiz:
            if id_quiz == quiz['id']:
                if not 'questions' in quiz:
                    quiz['questions'] = []
                quiz['questions'].append(question)
                del question['user_id']
                if user_resp is None:
                    question['user_response_valid'] = None
                elif opt_resp == user_resp:
                    question['user_response_valid'] = True
                else:
                    question['user_response_valid'] = False
                break
        #Add [options] in questions
        if not 'options' in question:
            question['options'] = []
        question['options'].append(opt_resp)
        question['options'].append(opt_2)
        question['options'].append(opt_3)
        question['options'].append(opt_4)
        #Delete field
        del question['response']
        del question['option_2']
        del question['option_3']
        del question['option_4']
        del question['quiz_id']
        del question['element_question']
        del question['value_question']
        del question['format']
    #Add [quiz] in user
    for quiz in user_info_quiz:
        theme_info = execute_query('SELECT t.label FROM quiz INNER JOIN themes t INNER JOIN themes_quiz tq on quiz.id = %s WHERE t.id = tq.themes_id', quiz['id'])
        quiz['themes'] = []
        for theme in theme_info:
            quiz['themes'].append(theme['label'])
        del quiz['id']
        grade = 0
        for question in quiz['questions']:
            if question['user_response_valid'] is True:
                grade += 1
        quiz['grade'] = grade

    user_info['quiz'] = user_info_quiz
    return user_info


def signup(body):
    email, password = body['email'], body['password']
    error, uuid = sign_up(email, password)
    if error is not None:
        return error, 404
    confirm_signup(email)
    return uuid


def import_countries():
    countries_in_db = execute_query('SELECT * FROM countries')
    countries = requests.get(Api.ALL_URL).json()
    insert_data = []
    added_countries = 0
    for country in countries:
        if (Api.INDEPENDENT in country and not country[Api.INDEPENDENT]) or any(elem['code'] == country[Api.CODE] for elem in countries_in_db):
            continue
        added_countries += 1
        subregion = country[Api.SUBREGION] if Api.SUBREGION in country else country[Api.REGION]
        has_coa = get_api_element(country, Api.COA) != NA
        insert_data.append((country[Api.CODE], get_random_difficulty(), country[Api.REGION], subregion, has_coa))

    if added_countries > 0:
        statement = 'INSERT INTO countries (code, difficulty, region, subregion, has_coa) VALUES (%s, %s, %s, %s, %s);'
        execute_many_queries(statement, insert_data)
    return "L'import s'est effectué correctement (" + str(added_countries) + " éléments importés)."


# For dev purpose only
def check_api_integrity():
    countries = requests.get(Api.ALL_URL).json()
    checked_countries = []
    for country in countries:
        element = {
            'code': get_api_element(country, Api.CODE),
            'name': get_api_element(country, Api.NAME),
            'capital': get_api_element(country, Api.CAPITAL),
            'flag': get_api_element(country, Api.FLAG),
            'coatOfArms': get_api_element(country, Api.COA)
        }
        checked_countries.append(element)
    return jsonify(checked_countries)
