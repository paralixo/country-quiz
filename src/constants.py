class Difficulties:
    EASY = 'Easy'
    MEDIUM = 'Medium'
    HARD = 'Hard'
    PROPERTIES = ['EASY', 'MEDIUM', 'HARD']


class Api:
    ALL_URL = 'https://restcountries.com/v3.1/all'
    COUNTRIES_BY_CODES = 'https://restcountries.com/v3.1/alpha/?codes='
    CODE = 'cca3'
    SUBREGION = 'subregion'
    REGION = 'region'
    INDEPENDENT = 'independent'
    CAPITAL = 'capital'
    FLAG = 'flags.png'
    NAME = 'name.common'
    COA = 'coatOfArms.png'


NUMBER_OF_QUESTIONS_IN_QUIZ = 10
NUMBER_OF_OPTIONS_IN_QUESTION = 4
NA = 'N/A'
