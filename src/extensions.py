from flask_mail import Mail
from flaskext.mysql import MySQL
from pymysql import cursors

mail = Mail()
mysql = MySQL(cursorclass=cursors.DictCursor)
