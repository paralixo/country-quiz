import random
from src.constants import Difficulties


def get_random_difficulty():
    difficulties_property = random.choice(Difficulties.PROPERTIES)
    return Difficulties.__getattribute__(Difficulties, difficulties_property)
