import random
import requests

from src.constants import Api, NUMBER_OF_QUESTIONS_IN_QUIZ, NUMBER_OF_OPTIONS_IN_QUESTION, NA
from src.db import execute_query


def get_api_element(api_response, element_name):
    element_names = element_name.split('.')
    element = api_response

    for name in element_names:
        try:
            element = element[name]
        except KeyError:
            element = NA
    if isinstance(element, list) and len(element) > 0:
        element = element[0]
    return element


def create_question(template, responses, countries_data):
    question = {}
    for i in range(NUMBER_OF_OPTIONS_IN_QUESTION):
        key = 'option_'
        code = responses[i]['code']
        country = get_country_data_by_code(countries_data, code)
        element_response = get_api_element(country, template['element_response'])
        if i == 0:
            element_question = get_api_element(country, template['element_question'])
            question['label'] = template['format'].replace('%value%', element_question)
            question['value_question'] = element_question
            question['response'] = element_response
            question['template_id'] = template['id']
        else:
            question[key+str(i+1)] = element_response
    return question


def get_question_properties(templates, countries):
    random_template_index = random.randint(0, len(templates) - 1)
    template = templates[random_template_index]
    need_coa = template['element_question'] == Api.COA or template['element_response'] == Api.COA
    options = []

    while len(options) < NUMBER_OF_OPTIONS_IN_QUESTION:
        random_country_index = random.randint(0, len(countries) - 1)
        country = countries[random_country_index]
        while country in options or (need_coa and not country['has_coa']):
            random_country_index = random.randint(0, len(countries) - 1)
            country = countries[random_country_index]
        options.append(country)

    return {'template': template, 'options': options}


def fetch_templates(body):
    theme_request = 'SELECT tq.id, format, element_question, t.element_response from templates_questions tq JOIN themes t on tq.theme_id = t.id '
    themes = body['themes']
    if len(themes) == 1:
        themes = str(themes[0])
        theme_request += 'WHERE t.label = %s;'
    elif len(themes) > 1:
        theme_request += "WHERE t.label IN {};".format(tuple(themes))
        themes = None
    templates = execute_query(theme_request, themes)
    return templates


def get_questions_properties(templates, countries):
    questions_properties = []
    existing_labels = []

    for i in range(NUMBER_OF_QUESTIONS_IN_QUIZ):
        question_properties = get_question_properties(templates, countries)
        question_label = (question_properties['template']['format'], question_properties['options'][0]['code'])
        attempts = 0
        while question_label in existing_labels and attempts <= 5:
            attempts += 1
            question_properties = get_question_properties(templates, countries)
            question_label = (question_properties['template']['format'], question_properties['options'][0]['code'])
        existing_labels.append(question_label)
        questions_properties.append(question_properties)
    return questions_properties


def get_all_countries_codes(questions_properties):
    codes = []
    for properties in questions_properties:
        options = properties['options']
        for option in options:
            if option not in codes:
                codes.append(option['code'])
    return ','.join(codes)


def get_country_data_by_code(all_countries_data, code):
    for country_data in all_countries_data:
        if get_api_element(country_data, Api.CODE) == code:
            return country_data
    return None


def fetch_countries(body):
    request_args = []
    country_request = "SELECT * FROM countries "
    difficulty = body['difficulty']
    difficulty_filter = 'difficulty LIKE %s'
    zone_filter = '(region LIKE %s OR subregion LIKE %s)'
    theme_zone = body['theme_zone']
    difficulty_is_not_empty = difficulty != ''
    zone_is_not_empty = theme_zone != ''
    filters = []

    if difficulty_is_not_empty:
        filters.append(difficulty_filter)
        request_args.append(difficulty)
    if zone_is_not_empty:
        filters.append(zone_filter)
        request_args.append(theme_zone)
        request_args.append(theme_zone)

    if len(filters) > 0:
        country_request += 'WHERE ' + ' AND '.join(filters)

    countries = execute_query(country_request, tuple(request_args))
    return countries


def fetch_data(body):
    list_questions = []
    countries = fetch_countries(body)
    templates = fetch_templates(body)

    if len(countries) == 0 or len(templates) == 0:
        return None

    questions_properties = get_questions_properties(templates, countries)
    countries_codes = get_all_countries_codes(questions_properties)
    countries_data = requests.get(Api.COUNTRIES_BY_CODES + countries_codes).json()
    for properties in questions_properties:
        template, options = properties['template'], properties['options']
        question = create_question(template, options, countries_data)
        list_questions.append(question)
    return list_questions
