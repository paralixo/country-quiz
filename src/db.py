from src.extensions import mysql


def execute_query(query, args=None):
    connect = mysql.connect()
    cursor = connect.cursor()
    cursor.execute(query, args)
    data = cursor.fetchall()
    connect.commit()
    return data


def execute_many_queries(statement, parameters):
    connect = mysql.connect()
    cursor = connect.cursor()
    cursor.executemany(statement, parameters)
    connect.commit()


def select_one(query, args=None):
    data = execute_query(query, args)
    if len(data) != 1:
        return None
    return data[0]


def get_uuid():
    connect = mysql.connect()
    cursor = connect.cursor()
    cursor.execute('SELECT UUID() as uuid')
    return cursor.fetchone()['uuid']


def insert_one(query, args=None):
    connect = mysql.connect()
    cursor = connect.cursor()
    cursor.execute(query, args)
    connect.commit()
    return cursor.lastrowid


def insert_many(statement, parameters):
    connect = mysql.connect()
    cursor = connect.cursor()
    ids = []
    for parameter in parameters:
        cursor.execute(statement, parameter)
        ids.append(cursor.lastrowid)
    connect.commit()
    return ids
