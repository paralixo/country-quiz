from flask_mail import Message
from src.extensions import mail


def confirm_signup(recipient_email):
    msg = Message('Inscription confirmée !', sender='countryquiz.api@gmail.com', recipients=[recipient_email])
    msg.body = "Votre compte a bien été créé, vous pouvez désormais accéder au service."
    mail.send(msg)
